import sys
from pymongo import MongoClient

from . import SimpleMrtProcessor
from .mongodb import MongoMrtImporter

print('Starting...')
with MongoClient() as client:
    with MongoMrtImporter(client['test'][sys.argv[2]], 10000) as mrt_importer:
        with SimpleMrtProcessor(sys.argv[1], mrt_importer) as mrt_processor:
            mrt_processor.process_all()
