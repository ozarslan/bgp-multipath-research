import mrtparse

from abc import ABC, abstractmethod

class BaseMrtImporter(ABC):
    """Base class for all database importers of MRT file format."""

    def __init__(self):
        """Initiate base class for all subclasses.

        All subclasses are required to call this constructor in their
        constructors.

        """
        pass

    @abstractmethod
    def add_route(self, prefix, subnet_mask, as_paths):
        """Add AS paths for given prefix.

        :param prefix: IP prefix.
        :param subnet_mask: Subnet mask of the prefix.
        :param as_paths: List of AS paths for the prefix.

        :type prefix: str
        :type subnet_mask: int
        :type as_paths: typing.Collection[typing.Sequence[int]]

        :rtype: None

        """
        pass

class BaseMrtProcessor(ABC):
    """Base class for all MRT processor."""

    def __init__(self, mrt_file):
        """Initiate MRT processor with an MRT file.

        GZIP or BZ compressed files are also supported thanks to `mrtparse`
        library.

        :param mrt_file: MRT file handle or name to be processed.
        :type mrt_file: str or typing.IO

        """

        self._reader = mrtparse.Reader(mrt_file)
        """MRT reader object."""

    def process_all(self):
        """Process all MRT entries in the file."""
        for mrt_iterator in self._reader:
            offset = mrt_iterator.f.tell()
            self.process_one(mrt_iterator.mrt, offset)

    @abstractmethod
    def process_one(self, mrt, offset):
        """Process given MRT entry.

        :param mrt: MRT entry.
        :param offset: Offset of the entry in the file.

        :type mrt: mrtparse.Mrt
        :type offset: int

        :rtype: None

        """
        raise NotImplementedError()

    def close(self):
        """Close MRT reader."""
        try:
            self._reader.close()
        except StopIteration:
            pass

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.close()

class SimpleMrtProcessor(BaseMrtProcessor):
    """A simple MRT processor storing AS paths per routes to database from the
    MRT file.

    It extract TABLE_DUMP_V2 type unicast IPv4 RIB prefix data with AS paths
    and stores it into database with offset information.

    """

    MRT_T_TABLE_DUMP_V2 = mrtparse.MRT_T['TABLE_DUMP_V2']
    MRT_ST_RIB_IPV4_UNICAST = mrtparse.TD_V2_ST['RIB_IPV4_UNICAST']
    BGP_ATTR_T_AS_PATH = mrtparse.BGP_ATTR_T['AS_PATH']

    def __init__(self, mrt_file, mrt_importer):
        """Initialize with given MRT file and database importer.

        :param mrt_file: MRT file handle or name to be processed.
        :param mrt_importer: MRT database importer.
        :type mrt_file: str or typing.IO
        :type mrt_importer: BaseMrtImporter

        """
        super().__init__(mrt_file)

        self.mrt_importer = mrt_importer

    def process_one(self, mrt, offset):
        from pprint import pprint
        # TODO: Convert prints to logging
        if mrt.type == self.MRT_T_TABLE_DUMP_V2:
            if mrt.subtype == mrtparse.TD_V2_ST['PEER_INDEX_TABLE']:
                self.peer_index = dict(
                    collector = mrt.peer.collector,
                    peers = [
                        dict(type=p.type, id=p.bgp_id, ip=p.ip, asn=p.asn)
                            for p in mrt.peer.entry]
                )
            elif mrt.subtype == self.MRT_ST_RIB_IPV4_UNICAST:
                ip_prefix, subnet_mask, as_paths = \
                    self.extract_as_paths_from_mrt_rib(mrt.rib)
                as_paths = [
                    list(map(int, as_path['val'])) for as_path in as_paths
                    if as_path['type'] == mrtparse.AS_PATH_SEG_T['AS_SEQUENCE']
                ]

                #  pprint((ip_prefix, subnet_mask, as_paths))
                self.mrt_importer.add_route(
                    ip_prefix, subnet_mask, as_paths, offset)
            else:
                print('Skipping some subtype: %d' % mrt.subtype)
        else:
            print('Skipping some type: %d' % mrt.type)

    @classmethod
    def extract_as_paths_from_mrt_rib(cls, rib):
        return rib.prefix, rib.plen, [
            as_path for entry in rib.entry
                for as_path in cls.extract_as_paths_from_rib_entry(entry)]

    @classmethod
    def extract_as_paths_from_rib_entry(cls, entry):
        for attr in entry.attr:
            if attr.type == cls.BGP_ATTR_T_AS_PATH:
                return attr.as_path

