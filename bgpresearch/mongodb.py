
from pymongo.operations import UpdateOne

from . import BaseMrtImporter

__all__ = ['MongoMrtImporter']

class MongoMrtImporter(BaseMrtImporter):
    """MRT file importer for MongoDB database."""

    def __init__(self, collection, write_cache_size = 1):
        """Initiate MRT importer to import data to MongoDB collection.

        :param collection: MongoDB collection.
        :param write_cache_size: Size of the write cache. Default is 1, meaning
            no cache. [Optional]

        :type collection: pymongo.collection.Collection
        :type write_cache_size: int

        """

        super().__init__()

        self.collection       = collection
        """Database collection to be used."""

        self.write_cache_size = write_cache_size
        """Size of the write cache."""

        self.write_cache = []
        """Cache of update requests."""

    def add_route(self, prefix, subnet_mask, as_paths, offset):
        # Find originator AS numbers.
        originators = list(set(map(lambda elm: elm[-1], as_paths)))

        self.write_cache.append(
            UpdateOne(
                # Get document id by ip and mask.
                { '_id': dict(ip=prefix, mask=subnet_mask) },

                # Add AS_PATHs and originators if they aren't already added.
                {
                    '$addToSet': {
                        'offsets': offset,
                        'as_paths': { '$each' : as_paths },
                        'originators' : { '$each' : originators }
                    }
                },

                # Create document if it's not already created.
                upsert = True
            )
        )

        # Flush if cache is full.
        if len(self.write_cache) >= self.write_cache_size:
            self.flush()

    def flush(self):
        """Write data stored in the cache.

        :rtype: None

        """

        # TODO: Output result to log
        self.collection.bulk_write(self.write_cache)
        self.write_cache.clear()

    def close(self):
        """Make sure cache is flushed before closing the object.

        Object can be closed multiple times as flushing is the only thing
        closing does currently. However, best practice is not to use a closed
        object again as it's not guaranteed that this will be expected behavior
        in the future.

        :rtype: None

        """

        self.flush()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.close()


