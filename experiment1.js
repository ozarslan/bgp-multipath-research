var Experiment1 = function() {}

Experiment1.prototype.groupByOriginators = function(from_collection, to_collection) {

  if(from_collection == to_collection || to_collection.exists()) {
    return false;
  }
  let mapper = function() {
    let doc = this;
    let route = doc._id.ip + '/' + doc._id.mask;
    if(doc.as_paths) {
      doc.as_paths.forEach((as_path) => {
        let originator = as_path[as_path.length - 1];
        let vantage_point = as_path[0];
        for(let i = as_path.length - 2; i > 0; i--)
        {
          let key = originator + ',' + as_path[i];
          let routes = {};
          let value = { routes_per_as_path : {} };
          let path_to_i = as_path.slice(i, as_path.length).join(',');

          routes[route] = doc._id;
          routes[route]['full_path'] = as_path;
          value.routes_per_as_path[path_to_i] = routes;

          emit(key, value);
        }
      });
    }
  }

  let reducer = function(key, values) {
    let reduced = values.reduce((acc, val) => {
      for(var as_path in val.routes_per_as_path)
      {
        routes = val.routes_per_as_path[as_path];
        acc_routes = acc.routes_per_as_path[as_path];
        if(acc_routes) {
          for(var route in routes) {
            acc_routes[route] = routes[route];
          }
        } else {
          acc.routes_per_as_path[as_path] = routes;
        }
      }
      return acc;
    });
    return reduced;
  }

  return from_collection.mapReduce(mapper, reducer, { out: to_collection.getName() });
}

Experiment1.prototype.findMultiplePaths = function(from_collection, to_collection) {
  if(from_collection == to_collection || to_collection.exists()) {
    return false;
  }
  let mapper = function()
  {
    let doc = this;
    if(doc.value) {
      let as_paths = doc.value.routes_per_as_path;

      // Summarize repetitions
      for(var as_path in as_paths)
      {
        summarized_as_path = as_path.split(',').reduce((acc, val) => {
          if(!acc[val]) {
            acc[val] = true;
            acc.value.push(val);
          }
          return acc;
        }, {value: []}).value.join(',');
        if(summarized_as_path != as_path) {
          if(as_paths[summarized_as_path]) {
            for(var route in as_paths[as_path])
            {
              if(!as_paths[summarized_as_path].hasOwnProperty(route)) {
                as_paths[summarized_as_path][route] = as_paths[as_path][route];
              }
            }
          } else {
            as_paths[summarized_as_path] = as_paths[as_path];
          }
          delete as_paths[as_path];
        }
      }

      as_paths = Object.keys(as_paths);
      if(as_paths.length > 1) {
        let expected = as_paths.map((as_path) => as_path.split(',')).reduce(
          (acc, as_path) => {
            if(acc) {
              let originator = as_path[as_path.length - 1];
              for(var i = as_path.length - 2; i >= 0; i--)
              {
                if(as_path[i] != originator) {
                  if(acc[as_path[i]]) {
                    return false;
                  } else {
                    acc[as_path[i]] = true;
                    return acc;
                  }
                }
              }
              return acc;
            }
          }, {}
        );
        if(!expected) {
          let value = as_paths.map((as_path) => {
            let d = {};
            d[as_path] = Object.keys(doc.value.routes_per_as_path[as_path]).reduce((acc, route) => {
              acc[route] = doc.value.routes_per_as_path[as_path][route]['full_path'];
              return acc;
            }, {});
            return d;
          });
          emit(doc._id, value);
        }
      }
    }
  }
  return from_collection.mapReduce(mapper, ()=>{}, { out: to_collection.getName() });
}

Experiment1.prototype.writeMultiplePaths = function(collection, limit)
{
  let docs = collection.find();
  if(limit) {
    docs = docs.limit(limit);
  }
  docs.forEach(doc => {
    print(doc._id);
    for(var j in doc.value)
    {
      for(var as_path in doc.value[j])
      {
        print('  ' + as_path);
        for(var route in doc.value[j][as_path])
        {
          print('    ' + route + ' : ' + doc.value[j][as_path][route]);
        }
      }
    }
  });
}
